
# RASH Todo app

Application using the RASH stack:

- Rust
- axum
- sqlx
- htmx

For HTML templating I used Maud.

## Running

Set up the SQLite database using `sqlx-cli`:

```bash
export DATABASE_URL="sqlite:todo.db"

sqlx db create

sqlx migrate run
```

Run the application:

```bash
cargo run
```

Now you can find the application running at http://localhost:3000

## Why

I saw [a video about the BETH stack](https://www.youtube.com/watch?v=cpzowDDJj24)
which combines Bun, Elysia, Turso and htmx into a stack that looks quite
nice to work with.

However, I like Rust more, so I decided to see what substitutions I
could make to get fairly close to the same developer experience,
particularly in terms of easy HTML templating in code.

The RASH stack, at least to me, seems to compare decently well. One
change is that Maud is used for HTML templating, which means the
resulting HTML isn't quite one-to-one with the language used in the
template, but it's not too bad.

I also opted to not use an ORM, preferring instead to deal with SQL as
SQL using sqlx. I have had bad experiences wrangling ORMs to do what I
want, so it feels simpler to just use the Database Access Object
pattern to wrap my direct SQL calls.
