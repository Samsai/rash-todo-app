
use sqlx::Executor;
use sqlx::SqlitePool;
use sqlx::Row;

use crate::Todo;

pub struct TodoDAO {
    db: SqlitePool
}

impl TodoDAO {
    pub async fn new(db_url: &str) -> TodoDAO {
        let dao = TodoDAO {
            db: SqlitePool::connect(db_url).await.expect("Failed to connect to database")
        };

        dao.initialize_tables().await;

        dao
    }

    pub async fn initialize_tables(&self) {
        self.db.execute("CREATE TABLE IF NOT EXISTS Todos ( id INTEGER PRIMARY KEY NOT NULL, content TEXT NOT NULL, completed BOOLEAN NOT NULL DEFAULT 0)").await.expect("Couldn't create tables");
    }

    pub async fn get_todos(&self) -> Vec<Todo> {
        let rows = sqlx::query("SELECT * FROM Todos").fetch_all(&self.db).await.expect("Failed to get Todos");

        let mut todos = vec![];

        for row in rows {
            let id: i64 = row.try_get("id").expect("Failed to get id");
            let content: String = row.try_get("content").expect("Failed to get content");
            let completed: i32 = row.try_get("completed").expect("Failed to get completed");

            todos.push(Todo { id, content, completed: completed == 1 });
        }

        return todos;
    }

    pub async fn get_todo_by_id(&self, id: i64) -> Option<Todo> {
        if let Ok(row) = sqlx::query("SELECT * FROM Todos WHERE id = ?").bind(id).fetch_one(&self.db).await {
            let id = row.try_get("id").expect("Failed to get id");
            let content = row.try_get("content").expect("Failed to get content");
            let completed: i32 = row.try_get("completed").expect("Failed to get completed");

            Some(Todo { id, content, completed: completed == 1})
        }
        else {
            None
        }
    }

    pub async fn create_todo(&self, content: String) -> Todo {
        let id = sqlx::query("INSERT INTO Todos ( content ) VALUES ( ? )").bind(content.clone()).execute(&self.db).await.expect("Failed to create new Todo").last_insert_rowid();

        Todo {
            id,
            content,
            completed: false
        }
    }

    pub async fn update_todo(&self, todo: &Todo) {
        sqlx::query("UPDATE Todos SET completed = ? WHERE id = ?").bind(todo.completed).bind(todo.id).execute(&self.db).await.expect("Failed to update Todo");
    }

    pub async fn delete_todo(&self, id: i64) {
        sqlx::query("DELETE FROM Todos WHERE id = ?").bind(id).execute(&self.db).await.expect("Failed to update Todo");
    }
}
