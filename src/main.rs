use std::sync::{Mutex, Arc};

use tokio;

use serde::Deserialize;

use maud::{html, Markup};

use axum::{Router, routing::get, routing::post, routing::delete};
use axum::http::StatusCode;
use axum::Form;
use axum::extract::Path;
use axum::extract::State;

mod db;
use db::TodoDAO;

struct Todo {
    pub id: i64,
    pub content: String,
    pub completed: bool
}

struct AppState {
    db: TodoDAO
}

#[tokio::main]
async fn main() {
    let db = db::TodoDAO::new("sqlite:todo.db").await;

    let app_state = Arc::new(AppState { db });

    let app = Router::new()
        .route("/", get(page))
        .route("/todos", get(todos))
        .route("/todo", post(new_todo))
        .route("/todo/:id", delete(delete_todo).patch(toggle_todo))
        .with_state(app_state);

    // run it with hyper on localhost:3000
    axum::Server::bind(&"0.0.0.0:3000".parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}

async fn page() -> Markup {
    html! {
        head {
            script src="https://unpkg.com/htmx.org@1.9.5" {}
        }

        body {
            h1 { "RASH To-do app"}

            ul id="todos" hx-trigger="load" hx-get="/todos" {
                
            }

            form hx-post="/todo" hx-target="#todos" hx-swap="beforeend"{
                h2 { "New to-do" }
                input type="text" name="content" {} br {}
                input type="submit" value="Create" {}
            }
        }
    }
}

fn todo_item(todo: &Todo) -> Markup {
    html! {
        li {
            (todo.content)
            @if todo.completed {
                input type="checkbox" checked hx-patch=(format!("/todo/{}", todo.id)) hx-target="closest li" { }
            } @else {
                input type="checkbox" hx-patch=(format!("/todo/{}", todo.id)) hx-target="closest li" { }
            }
            button hx-delete=(format!("/todo/{}", todo.id)) hx-target="closest li" hx-swap="delete" { "Delete"}
        }
    }
}

async fn todos(
    State(state): State<Arc<AppState>>
) -> Markup {
    let todos = state.db.get_todos().await;

    html! {
        @for todo in todos.iter() {
            (todo_item(todo))
        }
    }
}

#[derive(Deserialize)]
struct NewTodo {
    content: String
}

async fn new_todo(
    State(state): State<Arc<AppState>>,
    Form(new_todo): Form<NewTodo>
) -> Markup {
    let new_todo_item = state.db.create_todo(new_todo.content).await;
    
    html! {
        (todo_item(&new_todo_item))
    }
}

async fn delete_todo(
    State(state): State<Arc<AppState>>,
    Path(id): Path<i64>
) -> StatusCode {
    state.db.delete_todo(id).await;

    StatusCode::OK
}

async fn toggle_todo(
    State(state): State<Arc<AppState>>,
    Path(id): Path<i64>
) -> Result<Markup, StatusCode> {
    if let Some(mut todo) = state.db.get_todo_by_id(id).await {
        todo.completed = !todo.completed;

        state.db.update_todo(&todo).await;

        Ok(todo_item(&todo))
    } else {
        Err(StatusCode::NOT_FOUND)
    }
}
